As GameFAQs no longer supports its markup, these are legacy projects. Both aim to highlight GameFAQs markup in different text editors.

language-ffaq is a simple grammar for Atom. This is the more sophisticated of the two projects.

NP-FFAQ-Highlighting is a generated XML for Notepad++ using its *Define your language* tool.

Both of these have example images.